import os

# Server socket
bind = '0.0.0.0:8000'

# Worker processes
workers = 8
max_requests = 2000

#   Logging
accesslog = os.environ.get('GUNICORN_ACCESS_LOG', '-')
errorlog = os.environ.get('GUNICORN_ERROR_LOG', '-')

# Server Mechanics

# etc...
