How to run
==========

```bash
# from project folder
docker-compose up -d
virtualenv env --python={full_path_to}/python3.6
source env/bin/activate
pip install -r requirements.txt
python3 manage.py migrate
python3 manage.py runserver
curl http://localhost:8000/ping/ # pong
celery -A mysite worker -l INFO
```

User case
=========

POST categories:

```
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"name": "Category 1","children": [{"name": "Category 1.1","children": [{"name": "Category 1.1.1","children": [{"name": "Category 1.1.1.1"},{"name": "Category 1.1.1.2"}]}]},{"name": "Category 1.2","children": [{"name": "Category 1.2.1"},{"name": "Category 1.2.2"}]},{"name": "Category 1.3","children": []}]}' \
  http://localhost:8000/categories/
```

Open `http://localhost:8000/categories/` to find a category id

Open `http://localhost:8000/categories/{id}` to see a category structure

How to run tests
================

```bash
python3 ./manage.py test
```

How to run using Docker + gunicorn:
===================================

```bash
docker build --no-cache -t am:1.0 -f Dockerfile .
docker run -d -p 8000:8000 --name=am am:1.0
curl http://localhost:8000/ping/ # pong
```