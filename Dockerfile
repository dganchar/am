FROM alpine:3.6
MAINTAINER Dalila Ganchar <danila.ganchar@gmail.com>

RUN apk apk update
RUN apk add --no-cache \
    python3 \
    python3-dev \
    postgresql-libs \
    gcc \
    musl-dev \
    postgresql-dev

WORKDIR /src

ENV GUNICORN_ERROR_LOG /src/gunicorn-error.log
ENV GUNICORN_ACCESS_LOG /src/gunicorn-access.log

RUN touch $GUNICORN_ERROR_LOG
RUN touch $GUNICORN_ACCESS_LOG

COPY ./ ./

RUN pip3 install pip -U
RUN pip3 install --no-cache-dir -r ./requirements.txt

EXPOSE 80 443

ENTRYPOINT ["gunicorn"]
CMD ["-c", "gunicorn_config.py", "mysite.wsgi"]
