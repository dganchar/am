from django.db import models


class Category(models.Model):

    name = models.CharField(max_length=50, unique=True)
    node_path = models.CharField(max_length=255, unique=True)
    depth = models.IntegerField()
    num_child = models.IntegerField()

    class Meta:
        db_table = 'categories'

    def increase_num_child(self):
        self.num_child = self.num_child + 1

    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name,
        }
