from django.test import TestCase
from nose_parameterized import parameterized

from categories.materialized_path import FloatMaterializedPath


class FloatMaterializedPathTestCase(TestCase):

    float_material_path = FloatMaterializedPath()

    @parameterized.expand([
        (
            '1.1',
            '1.2'
        ),
        (
            '1.2.3.3',
            '1.2.3.4'
        ),
    ])
    def test_append_node(self, node_path, expected):
        result = self.float_material_path.get_sibling_path(node_path)
        self.assertEqual(result, expected)

    @parameterized.expand([
        (
            '1',
            '1.1'
        ),
        (
            '1.2.3',
            '1.2.3.1'
        ),
    ])
    def test_calculate_child_path(self, node_path, expected):
        result = self.float_material_path.get_child_path(node_path)
        self.assertEqual(result, expected)

    @parameterized.expand([
        (
            '1.18.3.5',
            '1.18.3'
        ),
        (
            '2.20',
            '2'
        ),
    ])
    def test_get_parent_path(self, node_path, expected):
        result = self.float_material_path.get_parent_path(node_path)
        self.assertEqual(result, expected)
