from unittest.mock import Mock

from django.test import TestCase

from categories.models import Category
from categories.repo.category_repo import PGCategoryRepo


class PGCategoryRepoTestCase(TestCase):

    def setUp(self):
        Category.objects.all().delete()

    def test_remove_tree_by_node_path(self):
        repo = PGCategoryRepo(Mock(
            get_sibling_path=Mock(side_effect=['11', '11']),
            get_child_path=Mock(side_effect=['11', '11']),
        ))
        root = Category(name='test1', node_path='1', depth=0, num_child=0)
        main = Category(name='test2', node_path='1.1', depth=0, num_child=0)
        child = Category(name='test3', node_path='1.1.1', depth=0, num_child=0)

        for i in [root, main, child]:
            i.save()

        repo.remove_tree_by_node_path(root.node_path)

        self.assertEqual(0, len(Category.objects.all()))

    def test_save_tree(self):
        repo = PGCategoryRepo(Mock(
            get_sibling_path=Mock(side_effect=['11', '11']),
            get_child_path=Mock(side_effect=['11', '11']),
        ))

        repo.save_tree({
            'name': 'test1',
            'children': [
                {'name': 'test2'}
            ]
        })

        self.assertEqual(2, len(Category.objects.all()))

    def test_get_category_siblings(self):
        category1 = Category(name='cat1', node_path='1', depth=1, num_child=0)
        category11 = Category(name='cat1.1', node_path='1.1', depth=2, num_child=0)
        category12 = Category(name='cat1.2', node_path='1.2', depth=2, num_child=0)
        category13 = Category(name='cat1.3', node_path='1.3', depth=2, num_child=0)
        category111 = Category(name='cat1.1.1', node_path='1.1.1', depth=3, num_child=0)
        category112 = Category(name='cat1.1.2', node_path='1.1.2', depth=3, num_child=0)

        for i in [
            category1,
            category11,
            category111,
            category112,
            category12,
            category13,
        ]:
            i.save()

        repo = PGCategoryRepo(Mock(get_parent_path=Mock(side_effect=[
            '',
            '1',
            '1.1'
        ])))

        result = repo.get_category_siblings(category1.id)
        self.assertDictEqual(
            {
                'id': category1.id,
                'name': category1.name,
                'children': [
                    {'id': category11.id, 'name': category11.name},
                    {'id': category12.id, 'name': category12.name},
                    {'id': category13.id, 'name': category13.name}
                ],
                'parents': [],
                'siblings': [],
            },
            result,
        )

        result = repo.get_category_siblings(category11.id)
        self.assertDictEqual(
            {
                'id': category11.id,
                'name': category11.name,
                'children': [
                    {'id': category111.id, 'name': category111.name},
                    {'id': category112.id, 'name': category112.name},
                ],
                'parents': [
                    {'id': category1.id, 'name': category1.name},
                ],
                'siblings': [
                    {'id': category12.id, 'name': category12.name},
                    {'id': category13.id, 'name': category13.name},
                ],
            },
            result,
        )

        result = repo.get_category_siblings(category112.id)
        self.assertDictEqual(
            {
                'id': category112.id,
                'name': category112.name,
                'children': [],
                'parents': [
                    {'id': category11.id, 'name': category11.name},
                ],
                'siblings': [
                    {'id': category111.id, 'name': category111.name},
                ],
            },
            result,
        )
