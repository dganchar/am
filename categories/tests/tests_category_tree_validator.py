from django.test import TestCase

from categories.validators.category_tree_validator import JsonSchemaValidator
from nose_parameterized import parameterized


class TreeJsonSchemaValidatorTestCase(TestCase):

    validator = JsonSchemaValidator()

    @parameterized.expand([
        (
            {'name': 'category 1'},
            0
        ),
        (
            {'name': 'category 2', 'children': []},
            0
        ),
        (
            {
                'name': 'category 3',
                'children': [
                    {'name': 'category 3.1', 'children': []},
                    {
                        'name': 'category 3.2',
                        'children': [
                            {'name': 'category 3.2.1'},
                        ]
                    },
                ]
            },
            0
        ),
        (
            {'undefined_key': 'value'},
            2
        ),
    ])
    def test_validate(self, data, expected):
        errors = self.validator.validate(data)

        self.assertEqual(len(errors), expected)
