from django.test import TestCase


class ViewsTestCase(TestCase):

    def test_ping(self):
        result = self.client.get('/ping/')

        self.assertEqual(result.content, b'pong')
        self.assertEqual(result.status_code, 200)
