import json

import inject
from django.http import JsonResponse, HttpResponseBadRequest, HttpResponseNotFound
from django.views import View

from categories.models import Category
from categories.repo.category_repo import ICategoryRepo
from categories.validators.category_tree_validator import ICategoryTreeValidator
from mysite.celery import app


class CategoriesView(View):

    _category_validator = inject.attr(ICategoryTreeValidator)

    def get(self, request):
        result = [
            {'id': i.id, 'name': i.name}
            for i in Category.objects.all()
        ]
        return JsonResponse(result, safe=False)

    def post(self, request):
        data = json.loads(request.body)
        errors = self._category_validator.validate(data)
        if errors:
            return HttpResponseBadRequest(' '.join(errors))

        task = app.send_task('categories.tasks.async_save_categories_tree', (data, ))

        return JsonResponse({'task_id': task.id}, status=201)


@inject.params(
    repo=ICategoryRepo
)
def category_view(request, category_id, repo: ICategoryRepo):
    data = repo.get_category_siblings(category_id)
    if not data:
        return HttpResponseNotFound('Category %s not found' % category_id)

    return JsonResponse(data)

