import abc


class IMaterializedPath:

    @abc.abstractmethod
    def get_child_path(self, node_path: str) -> str:
        """
        :return: node path
        """

    @abc.abstractmethod
    def get_sibling_path(self, node_path: str) -> str:
        """
        :return: node path
        """

    @abc.abstractmethod
    def get_parent_path(self, node_path: str) -> str:
        """
        :return: node path
        """


class FloatMaterializedPath(IMaterializedPath):

    PATH_DELIMITER = '.'

    def get_sibling_path(self, node_path: str) -> str:
        path_items = node_path.split(self.PATH_DELIMITER)
        child_seq = int(path_items.pop()) + 1
        path_items.append(str(child_seq))

        return self.PATH_DELIMITER.join(path_items)

    def get_child_path(self, node_path: str) -> str:
        return self.PATH_DELIMITER.join([node_path, '1'])

    def get_parent_path(self, node_path: str) -> str:
        path_items = node_path.split(self.PATH_DELIMITER)
        path_items.pop()

        return self.PATH_DELIMITER.join(path_items)
