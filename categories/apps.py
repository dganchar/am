from django.apps import AppConfig

from categories.injector import configure


class CategoriesConfig(AppConfig):
    name = 'categories'

    def ready(self):
        configure()
