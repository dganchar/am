import inject

from categories.materialized_path import IMaterializedPath, FloatMaterializedPath
from categories.repo.category_repo import ICategoryRepo, PGCategoryRepo
from categories.validators.category_tree_validator import ICategoryTreeValidator, JsonSchemaValidator


def _categories_binder(binder):
    mat_path = FloatMaterializedPath()
    repo = PGCategoryRepo(mat_path)

    binder.bind(ICategoryTreeValidator, JsonSchemaValidator())
    binder.bind(IMaterializedPath, mat_path)
    binder.bind(ICategoryRepo, repo)


def configure():
    def config(binder):
        binder.install(_categories_binder)

    inject.clear_and_configure(config)
