import inject

from categories.models import Category
from categories.repo.category_repo import ICategoryRepo


def sync_save_categories_tree(data: dict):
    category_repo = inject.instance(ICategoryRepo)  # type: ICategoryRepo
    category = Category.objects.filter(name=data['name']).first()
    if category:
        category_repo.remove_tree_by_node_path(category.node_path)

    category_repo.save_tree(data)
