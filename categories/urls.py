from django.urls import path

from categories.views import CategoriesView, category_view

urlpatterns = [
    path('', CategoriesView.as_view()),
    path('<int:category_id>', category_view),
]
