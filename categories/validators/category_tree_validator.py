import abc

from jsonschema import Draft4Validator


class ICategoryTreeValidator:

    @abc.abstractmethod
    def validate(self, data: dict) -> list:
        """
        :param data: {
            'name': 'Category 1',
            'children': [
            {
                'name': 'Category 1.1',
                'children': [...]
            },
        }
        :return: errors
        """


_JSON_SCHEMA = {
    "$schema": "https://json-schema.org/schema#",
    'definitions': {
        'category': {
            'type': 'object',
            'required': ['name'],
            'additionalProperties': False,
            'properties': {
                'name': {'type': 'string'},
                'children': {
                    'type': 'array',
                    'items': {'$ref': '#/definitions/category'},
                    'default': []
                }
            }
        }
    },
    'type': 'object',
    'properties': {
        'category': {'$ref': '#/definitions/category'}
    },
    'additionalProperties': False,
}


class JsonSchemaValidator(ICategoryTreeValidator):

    def validate(self, data: dict) -> list:
        validator = Draft4Validator(_JSON_SCHEMA)
        errors = []
        for error in validator.iter_errors({'category': data}):
            errors.append(str(error))

        return errors
