import abc
import logging

import inject
from django.apps import apps
from django.db import transaction

from categories.materialized_path import IMaterializedPath

logger = logging.getLogger(__name__)


class ICategoryRepo:

    @abc.abstractmethod
    def save_tree(self, data: dict):
        """
        :param data: {
            "name": "category 1",
            "children": [
                {
                    "name": "category 1.1",
                    "children": [...]
                },
                ...
            ]
        }
        """
        pass

    @abc.abstractmethod
    def remove_tree_by_node_path(self, node_path: str):
        pass

    @abc.abstractmethod
    def get_category_siblings(self, category_id: int) -> dict:
        """
        :return: {
            "id": category_id,
            "name": category_name,
            "parents": [{"id": ..., "name": ...}, ...],
            "children": [{"id": ..., "name": ...}, ...],
            "siblings": [{"id": ..., "name": ...}, ...],
        }
        """
        pass


class PGCategoryRepo(ICategoryRepo):

    @inject.params(
        mat_path=IMaterializedPath
    )
    def __init__(self, mat_path: IMaterializedPath) -> None:
        self._mat_path = mat_path

    def _create_categories_from_tree(self, category_data: dict, depth=1, node_path='1'):
        category_cls = apps.get_model('categories', 'Category')
        category = category_cls(
            name=category_data['name'],
            depth=depth,
            num_child=0,
            node_path=node_path
        )

        current_path = None
        for child in category_data.get('children', []):
            category.increase_num_child()
            if current_path:
                current_path = self._mat_path.get_sibling_path(current_path)
            else:
                current_path = self._mat_path.get_child_path(node_path)

            self._create_categories_from_tree(child, depth + 1, current_path)

        category.save()

    def save_tree(self, data: dict):
        logger.debug('Saving categories tree. %s', data)

        with transaction.atomic():
            self._create_categories_from_tree(data)

    def remove_tree_by_node_path(self, node_path: str):
        from django.db import connection

        logger.debug('Removing categories tree. path: %s', node_path)
        cursor = connection.cursor()

        cursor.execute("""
            DELETE FROM categories
             WHERE node_path = %s OR node_path LIKE %s
         """, [node_path, node_path + '.%'])

    def get_category_siblings(self, category_id: int) -> dict:
        category_cls = apps.get_model('categories', 'Category')
        category = category_cls.objects.filter(id=category_id).first()
        if not category:
            return {}

        parent_depth, children_depth = category.depth - 1, category.depth + 1
        parent_path = self._mat_path.get_parent_path(category.node_path)

        params = [parent_path + '%', parent_depth, children_depth, category.node_path]
        query = """
            SELECT *
              FROM categories
             WHERE node_path LIKE %s
               AND (depth >= %s OR depth <= %s)
               AND node_path != %s
        """

        parents = []
        children = []
        siblings = []
        for record in category_cls.objects.raw(query, params):
            if record.depth == parent_depth:
                parents.append(record.to_dict())
            if (
                record.node_path.startswith(category.node_path) and
                record.depth == children_depth
            ):
                children.append(record.to_dict())
            if record.depth == category.depth:
                siblings.append(record.to_dict())

        result = category.to_dict()
        result['parents'] = parents
        result['children'] = children
        result['siblings'] = siblings

        return result
