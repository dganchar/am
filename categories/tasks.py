from __future__ import absolute_import, unicode_literals

from celery import shared_task
from categories.sync_tasks import sync_save_categories_tree


@shared_task
def async_save_categories_tree(categories_tree: dict):
    sync_save_categories_tree(categories_tree)
